# ClothingShop [![pipeline status](https://gitlab.com/DimaSerbenyuk/clothingshop/badges/main/pipeline.svg)](https://gitlab.com/DimaSerbenyuk/clothingshop/-/commits/main) [![Coverage report](https://gitlab.com/DimaSerbenyuk/clothingshop/badges/main/coverage.svg?job=coverage)](https://gitlab.com/DimaSerbenyuk/clothingshop/coverage)

python -m pip install --user virtualenv

python3 -m venv env

source env/bin/activate

pip freeze

pip freeze > requirements.txt

python manage.py runserver

#check for migrations
python manage.py makemigrations

#make migrations
python manage.py migrate

python -m pip install --upgrade pip

pip install -r requirements.txt


gpg --gen-key

gpg --list-keys

pub   ed25519 2022-12-13 [SC] [   годен до: 2024-12-12]

      7*****************FE352


gpg --output postgres.13.12.sql.bz2 --decrypt postgres--13-12-2022--16-42-52.sql.bz2.gpg

bzip2 -d postgres.13.12.sql.bz2

psql postgres      

psql postgres < postgres.13.12.sql


kubectl delete pv  postgres-main-pv-volume  --grace-period=0 --force