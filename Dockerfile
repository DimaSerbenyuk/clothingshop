
FROM python:3.9-slim as builder
ENV PYTHONUNBUFFERED=1

RUN pip install -U pip setuptools wheel

WORKDIR /wheels
COPY requirements.txt /requirements.txt
RUN pip wheel -r /requirements.txt


FROM python:3.9-slim
ENV PYTHONUNBUFFERED=1

# install psycopg dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \ 
    python-dev \
    libpng-dev \
    cmake \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /wheels /wheels
RUN pip install -U pip setuptools wheel \
      && pip install /wheels/* \
      && rm -rf /wheels \
      && rm -rf /root/.cache/pip/*
RUN pip install psycopg2

WORKDIR /clothingshop
COPY . .

EXPOSE 8000
ENV PYTHONPATH /clothingshop

#CMD ["gunicorn", "-c", "gunicorn.py", "clothingshop.wsgi:application"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--workers", "1", "clothingshop.wsgi:application"]
# run entrypoint.prod.sh
#ENTRYPOINT ["/clothingshop/docker/wsgi-entrypoint.sh"]
